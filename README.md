# Test d'Ishihara

## Comment ça marche ?

On possède une arène où des robots, possédant une caméra, doivent se déplacer pour trouver des cibles. Les cibles sont des objets de couleurs différentes. Les robots doivent donc détecter les cibles et les identifier.

### Périmètre

Il faut trouver les cibles et les identifier. Les cibles sont des objets de couleurs différentes. Les robots doivent donc détecter les cibles et les identifier parmi une sélection de couleurs données.

## Ressources

- Robot possédant une caméra
- Arène
- Cibles de couleurs différentes
- PyCharm / Python
- [OVA](https://jusdeliens.com/ovapythonapi/): API du robot
- ± 6 heures

## Organisation

- Récupérer et stocker les images de la caméra du robot (1h)
- Classifier les images par couleur (45min)
- Mettre les données classées dans un fichier CSV pour une réutilisation plus rapide (30min)
- Réfléchir aux variables métriques (30min)
- Créer un SVM pour classifier les images (1h30)
- Tester et adapter le modèle (1h30)

## How to build

```bash
# Clone the repository
$ git clone https://gitlab.com/Telio3/ishihara
$ cd ishihara

# Install dependencies
$ pip install -r requirements.txt
```

Ajouter le fichier [ova.py](https://jusdeliens.com/ovapythonapi/) à la racine du projet.

## How to run

```bash
# Run the program
$ python main.py

# Suivez les instructions de la console
```

## Caractérisation
### Matrice de confusion du model
![CM](src/cm.png "Matrice de confusion")

On peut voir que le modèle entrainé est très performant. Il est capable de détecter les signaux de couleur avec une précision de 100% sur les 6 couleurs.
Après essai, le modèle a un certain faible pour différencier le vert et le jaune.

### Causalité du phénomène à classifier
Le phénomène à classifier est la détection de signaux de couleur. Ces derniers sont disposés sur une arène et le robot doit les détecter et les identifier grâce à sa caméra.

### Explication des métriques input
Afin caractériser les signaux de couleur, j'ai décidé de prendre en compte 1 paramètre :
    - La couleur sur un pixel de la bordure du cercle

### Classes à prédire output
Les classes à prédire sont les couleurs des signaux.\
Il y a 6 couleurs possibles :
    - Rouge
    - Jaune
    - Vert
    - Cyan
    - Bleu
    - Magenta


## Crédits

- [Télio](https://gitlab.com/Telio3)
- Licence: [CC BY-NC-SA 4.0](https://creativecommons.org/licenses/by-nc-sa/4.0/)
import seaborn as sns
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn.svm import SVC
from ova import *
import os
import shutil
import uuid
from PIL import Image


class MyRobot(OvaClientMqtt):
    def __init__(self):
        super().__init__(id="ova1097bdcc7ca1", arena="ishihara", username="", password="", server="192.168.10.103",
                         port=1883)

        self.timeBetweenFrames = 2
        self.trainingTime = 2
        self.categoryColors = [(255, 0, 0), (255, 255, 0), (0, 255, 84), (0, 255, 255), (0, 0, 255), (254, 0, 255),
                               (0, 0, 0)]
        self.categoryNames = ['red', 'yellow', 'green', 'cyan', 'blue', 'magenta', 'black']
        self.tones = [261, 391, 349, 329, 293, 523, 0]
        self.currentCategory = 0
        self.predictedRGB = None
        self.svm = None
        self.xTaken = 218
        self.yTaken = 230
        self.isTesting = False
        self.timeWhereLedIsUpdated = time.time()

    def _onImageReceived(self, img: Image):
        # Enregistre l'image dans le dossier data dans le sous-dossier correspondant à la catégorie
        if not self.isTesting:
            img.save('data/' + self.categoryNames[self.currentCategory] + '/' + str(uuid.uuid4()) + '.jpeg')

    def prompt(self):
        print("1. Get Data")
        print("2. Train")
        print("3. Test")
        print("4. Quit")
        return int(input("Choice: "))

    def connectRobot(self):
        while not self.isConnected():
            print("🔴", self.getRobotId(), "déconnectée")
            time.sleep(1)

        print("🟢", self.getRobotId(), "connectée")

    def getData(self):
        self.isTesting = False

        self.connectRobot()

        # Input pour le temps d'entrainement
        while True:
            try:
                self.trainingTime = int(input("Training time (in minutes): "))
                break
            except ValueError:
                print("Please enter a number")

        # Créer le dossier data et les sous-dossiers pour chaque catégorie
        shutil.rmtree('data', ignore_errors=True)
        os.mkdir('data')

        for category in self.categoryNames:
            os.mkdir('data/' + category)

        self.connectRobot()

        # Appuyer sur entrée pour commencer l'entrainement
        input("Press enter to start training for " + self.categoryNames[0])

        # Enregistrement de l'heure de début
        start_time = time.time()

        # Tant que le temps d'entrainement n'est pas écoulé
        while time.time() - start_time < self.trainingTime * 60:
            # Appelle la fonction robot.update() pour récupèrer l'image de la caméra
            self.update()

            # Efface la console. Utiliser 'cls' sur windows, 'clear' sous unix
            os.system('cls')

            # Si le robot est déconnecté
            if not self.isConnected():
                print("🔴", self.getRobotId(), "déconnectée")

            # Si le robot est connecté
            else:
                # Affiche l'état et les capteurs du robot
                print("🟢", self.getRobotId(), "connectée")

                self.currentCategory = (self.currentCategory + 1) % len(self.categoryNames)

            time.sleep(self.timeBetweenFrames)

        self.train()

    def train(self):
        self.isTesting = False

        self.connectRobot()

        data = []
        labels = []

        # Récupère les données des images et les met dans un tableau avec leurs labels associés
        for color in self.categoryNames:
            for filename in os.listdir('data/' + color):
                img = Image.open('data/' + color + '/' + filename)
                data.append(img.getpixel((self.xTaken, self.yTaken)))
                labels.append(color)

        # Mettre les données dans un fichier csv
        with open('data.csv', 'w') as f:
            for i in range(len(data)):
                f.write(str(data[i][0]) + ',' + str(data[i][1]) + ',' + str(data[i][2]) + ',' + str(labels[i]) + '\n')

        # Préparation des données
        X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, random_state=42)

        # Entrainement du modèle
        self.svm = SVC(kernel='linear')
        self.svm.fit(X_train, y_train)

        # Évaluation du modèle
        y_pred = self.svm.predict(X_test)

        self.showPlots(y_test, y_pred)

    def showPlots(self, y_test, y_pred):
        print('Precision:', precision_score(y_test, y_pred, average='weighted'))
        print('Recall:', recall_score(y_test, y_pred, average='weighted'))
        print('F1 score:', f1_score(y_test, y_pred, average='weighted'))

        plt.plot(y_test, color='blue', label='Actual')
        plt.plot(y_pred, color='red', label='Predicted')
        plt.legend()
        plt.show()

        # Matrice de confusion
        cm = confusion_matrix(y_test, y_pred)

        # Affichage de la matrice de confusion
        plt.figure(figsize=(10, 7))
        sns.heatmap(cm, annot=True)
        plt.xlabel('Predicted')
        plt.ylabel('Truth')
        plt.show()

    def test(self):
        self.isTesting = True

        self.connectRobot()

        # Récupère les données du fichier csv
        data = []
        labels = []

        # Récupère les données des images depuis le fichier csv
        with open('data.csv', 'r') as f:
            for line in f:
                values = line.split(',')
                data.append([int(values[0]), int(values[1]), int(values[2])])
                labels.append(values[3].replace('\n', ''))
        
        # Préparation des données
        X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.2, random_state=42)

        # Entrainement du modèle
        self.svm = SVC(kernel='linear')
        self.svm.fit(X_train, y_train)

        while True:
            # Appelle la fonction robot.update() pour récupèrer l'image de la caméra
            self.update()

            # Efface la console. Utiliser 'cls' sur windows, 'clear' sous unix
            os.system('cls')

            # Si le robot est déconnecté
            if not self.isConnected():
                print("🔴", self.getRobotId(), "déconnectée")

            # Si le robot est connecté
            else:
                # Affiche l'état et les capteurs du robot
                print("🟢", self.getRobotId(), "connectée")
                print(
                    f"🔋batterie {self.getBatteryLevel()}% 💡avant {self.getFrontLuminosityLevel()}% 💡arrière {self.getBackLuminosityLevel()}%")

                r, g, b = self.getImagePixelRGB(self.xTaken, self.yTaken)

                y_pred = self.svm.predict([[r, g, b]])

                print(y_pred[0])

                index = self.categoryNames.index(y_pred[0])
                predictedRGB = self.categoryColors[index]

                if self.predictedRGB != predictedRGB and time.time() - self.timeWhereLedIsUpdated > 0.5:
                    print("🎵")
                    self.timeWhereLedIsUpdated = time.time()
                    self.predictedRGB = predictedRGB
                    self.setLedColor(self.predictedRGB[0], self.predictedRGB[1], self.predictedRGB[2])
                    self.playMelody([(self.tones[index], 500)])


def main():
    myRobot = MyRobot()

    while True:
        choice = myRobot.prompt()
        if choice == 1:
            myRobot.getData()
        elif choice == 2:
            myRobot.train()
        elif choice == 3:
            myRobot.test()
        elif choice == 4:
            break
        else:
            print("Invalid choice")


if __name__ == "__main__":
    main()
